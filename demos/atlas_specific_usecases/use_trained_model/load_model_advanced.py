import os
import sys
import json
import pickle
import joblib
import argparse
import numpy as np
import keras as ks
import deepcalo as dpcal

# Argument parsing
parser = argparse.ArgumentParser()
parser.add_argument('--model_path', help='Path of JSON model to be loaded.', default=None, type=str)
parser.add_argument('--weights_path', help='Path of HDF5 weights to be loaded.', default=None, type=str)
parser.add_argument('--data_path', help='Path to data', default=None, type=str)
parser.add_argument('-v','--verbose', help='Verbose output, 0, 1 or 2, where 2 is less verbose than 1.', default=2, type=int)
args = parser.parse_args()

model_path = args.model_path
weights_path = args.weights_path
data_path = args.data_path
verbose = args.verbose

params_dir = 'models/model_ER_central_0-100GeV_photons_images_scalars/'

# ======================================================
# Load model
# ======================================================
# Model reconstruction from JSON
# We need to include the custom FiLM layer, as implemented in DeepCalo.
with open(model_path, 'r') as model_json:
    arch = json.load(model_json)
    model = ks.models.model_from_json(arch, custom_objects={'FiLM': dpcal.layers.FiLM()})
if verbose:
    print(f'Model architecture loaded from {model_path}.')

# Inspect the structure of the model
model.summary()

# Load weights
model.load_weights(weights_path)
if verbose:
    print(f'Weights loaded from {weights_path}.')

# ======================================================
# Load data
# ======================================================
# Load the first point of the training set, no points of the validation set (e.g. 'val':10), and the first 100 points of the test set.
# Choose None instead of a number to load all points in that set (e.g. 'test':None).
n_points = {'train':1, 'test':1e2}

# Load data parameters associated with the model
with open(os.path.join(params_dir,'dataparams.pkl'), 'rb') as f:
    data_params = pickle.load(f)

data = dpcal.utils.load_atlas_data(data_path, n_points=n_points, **data_params, verbose=verbose)

# ======================================================
# Standardize scalar variables
# ======================================================
for name in data_params['scalar_names']:
    with open(os.path.join(params_dir,f'scalers/scaler_{name}.jbl'), 'rb') as f:
        scaler = joblib.load(f)

        var_ind = data_params['scalar_names'].index(name)
        for set_name in data:
            data[set_name]['scalars'][:,var_ind] = np.squeeze(scaler.transform(np.expand_dims(data[set_name]['scalars'][:,var_ind],1)))
            if verbose:
                print(f'Min and max of {name} in {set_name} set after standardization: '
                      f'{data[set_name]["scalars"][:,var_ind].min(), data[set_name]["scalars"][:,var_ind].max()}')

# ======================================================
# Predict
# ======================================================
# Predict on the 100 first test datapoints (values are in GeV)
if verbose:
    print('Predicting.')

# Choose which set to predict on
pred_set = 'test'

# Organize input data
# It is essential that inputs are added in this order, as it is what the loaded
# model expects.
inputs = []

# Images
if 'images' in data[pred_set] and dpcal.utils.boolify(data[pred_set]['images']):
    for img_name in data[pred_set]['images']:
        inputs.append(data[pred_set]['images'][img_name])

# Scalars
if 'scalars' in data[pred_set] and dpcal.utils.boolify(data[pred_set]['scalars']):
    inputs.append(data[pred_set]['scalars'])

# Tracks
if 'tracks' in data[pred_set] and dpcal.utils.boolify(data[pred_set]['tracks']):
    inputs.append(data[pred_set]['tracks'])

# Multiply output with
if 'multiply_output_with' in data[pred_set] and dpcal.utils.boolify(data[pred_set]['multiply_output_with']):
    inputs.append(data[pred_set]['multiply_output_with'])

# Predict
preds = model.predict(inputs).flatten()

if verbose:
    print('Predictions:')
    print(preds)

# We could here also apply a bias correction, which is saved in the class found
# at os.path.join(params_dir, 'bias_correction_class_FLAGS:rm_bad_reco:True_lhCut:p_photonIsLooseEM.jbl'),
# but this is not essential, and will not be done here. Instead, example usage will
# be provided in a future update.
