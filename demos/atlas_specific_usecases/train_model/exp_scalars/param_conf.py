def get_params():
    """
    Returns a dictionary containing parameters to be passed to the model
    container.

    Please see the README for documentation.
    """

    params = {
          # Training
          'epochs'                     : 300,
          'batch_size'                 : 1024,
          'loss'                       : 'logcosh',
          'metrics'                    : ['mae'],
          'optimizer'                  : 'Nadam', # '{'class_name':'SGD', 'config':{'nesterov':True, 'momentum':0.75}},
          'lr_finder'                  : {'use':True,
                                          'scan_range':[1e-5, 1e-2],
                                          'epochs':1,
                                          'prompt_for_input':False},
          'lr_schedule'                : {'name':'CLR', # 'OneCycle',
                                          'range':[5e-4, 7e-2],
                                          'step_size_factor':3, # 2.25,
                                          'kwargs':{}}, # {'base_m':0.65,
                                                   # 'max_m':0.75,
                                                   # 'cyclical_momentum':True}},
          'auto_lr'                    : True,

          # Misc.
          'use_earlystopping'          : False,
          'restore_best_weights'       : True,
          'pretrained_model'           : {'use':False,
                                          'weights_path':'path/to/weights',
                                          'params_path':None,
                                          'layers_to_load':['top', 'cnn', 'FiLM_generator', 'scalar_net'],
                                          'freeze_loaded_layers':False},
          'upsampling'                 : {'use':True,
                                          'wanted_size':(56,55)},

          # Submodels
          'top'                        : {'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'units':[256,256,1],
                                          'final_activation':'relu'},
          'cnn'                        : {'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'block_depths':[1,2,2,2,2],
                                          'n_init_filters':16,
                                          'downsampling':'maxpool',
                                          'min_size_for_downsampling':6},
          'scalar_net'                 : {'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'units':[256],
                                          'connect_to':['FiLM_gen']},
          'FiLM_gen'                   : {'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'use':True,
                                          'units':[512,1024]},
          }

    return params
