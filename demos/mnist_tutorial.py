import os
import copy
import argparse
import numpy as np
import keras as ks
from keras.datasets import mnist
import deepcalo as dpcal

# ==============================================================================
# Argument parsing
# ==============================================================================
parser = argparse.ArgumentParser()
parser.add_argument('-g','--gpu', help='Which GPU(s) to use, e.g. "0" or "1,3".', default='', type=str)
parser.add_argument('--exp_dir', help='Directory of experiment, e.g. "my_exp/".', default='./', type=str)
parser.add_argument('--save_figs', help='Save figures.', default=True, type=dpcal.utils.str2bool)
parser.add_argument('-v','--verbose', help='Verbose output, 0, 1 or 2, where 2 is less verbose than 1.', default=2, type=int)
args = parser.parse_args()

gpu_ids = args.gpu
exp_dir = args.exp_dir
save_figs = args.save_figs
verbose = args.verbose

# Set which GPU(s) to use
os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_ids

# ==============================================================================
# Hyperparameters
# ==============================================================================
# Set hyperparameters that should be different from the default values
params = {'metrics':['categorical_accuracy'],
          'top':{'normalization':'batch',
                 'activation':'leakyrelu',
                 'units':[16,16,10],
                 'final_activation':'softmax'},
          'cnn':{'normalization':'batch',
                 'activation':'leakyrelu',
                 'downsampling':'maxpool',
                 'block_depths':[1,1,1,1]}}

# Use the chosen parameters where given, and use the default parameters otherwise
params = dpcal.utils.merge_dicts(dpcal.utils.get_default_params(), params, in_depth=True)

# Set learning rate based on optimizer and batch size
if params['auto_lr']:
    params = dpcal.utils.set_auto_lr(params)

# Integrate the hyperparameters given from the command line
params['n_gpus'] = 0 if not gpu_ids else len(gpu_ids.split(','))

# ==============================================================================
# Data
# ==============================================================================
def get_data():
    """
    A function made so that it is easy to see what part of the code is handling
    the data and its preparation, which is very specific to this tutorial.
    """

    # Load example data (here MNIST)
    # The data, split between train and test sets
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    # Split test into test and val
    x_test, x_val = np.array_split(x_test,2)
    y_test, y_val = np.array_split(y_test,2)

    # Organize into dictionary
    # This is the data structure that is expected by the ModelContainer class.
    # Here, we only have a single kind of image, which we will call 'mnist'.
    # The model constructed by the ModelContainer instance will automatically adapt
    # to the input dimensions of the data that is fed to it.
    data = {'train':{'images':{'mnist':x_train},
                     'targets':y_train},
            'val':{'images':{'mnist':x_val},
                   'targets':y_val},
            'test':{'images':{'mnist':x_test},
                    'targets':y_test}}

    # Prepare the images
    img_rows, img_cols = 28, 28

    for set_name in data:
        data[set_name]['images']['mnist'] = data[set_name]['images']['mnist'].reshape(data[set_name]['images']['mnist'].shape[0], img_rows, img_cols, 1)
        data[set_name]['images']['mnist'] = data[set_name]['images']['mnist'].astype('float32')
        data[set_name]['targets'] = ks.utils.to_categorical(data[set_name]['targets'])
        data[set_name]['images']['mnist'] /= 255

    return data

# Get the data for this tutorial
data = get_data()

# ==============================================================================
# Logging directories
# ==============================================================================
# Make directories for saving figures and models
# dirs is a dictionary of paths
dirs = dpcal.utils.create_directories(exp_dir, params['epochs'], log_prefix=gpu_ids)

# ==============================================================================
# Create and use model
# ==============================================================================
# Instantiate model container (with self.model in it)
# This is where all the magic happens ...
# have a look in model_container.py to get an idea of what is going on
mc = dpcal.ModelContainer(data=data,
                          params=params,
                          dirs=dirs,
                          save_figs=save_figs,
                          verbose=verbose)

# Save hyperparameters (with some additional information)
params_for_saving = copy.deepcopy(mc.params)
params_for_saving['n_params'] = mc.model.count_params()
dpcal.utils.save_dict(params_for_saving, dirs['log'] + 'hyperparams.txt', save_pkl=True)

# Train model
mc.train()

# Evaluate (predicting and evaluating on test or validation set)
if not hasattr(mc,'evaluation_scores'):
    mc.evaluate()

# Print results
if verbose:
    print('Evaluation scores:')
    print(mc.evaluation_scores)
